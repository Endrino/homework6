package DopTasks;

import java.util.Scanner;

public class ArmstrongNumber {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int checkNumber = scanner.nextInt();
        System.out.println(isArmstrong(checkNumber));
    }

    public static boolean isArmstrong (int checkNumber) {
        int sum = 0;
        int n = checkNumber;
        int x = (int) (Math.log10(n) + 1);
        while (n > 0) {
            int i = n % 10;
            sum += Math.pow(i, x);
            n /= 10;
        }
        return checkNumber == sum;
    }

}
