package DopTasks;

public class PrimeNumber {
    public static void main(String[] args) {
        System.out.println(isPrime(1));
        System.out.println(isPrime(2));
        System.out.println(isPrime(3));
        System.out.println(isPrime(4));
        System.out.println(isPrime(5));
        System.out.println(isPrime(6));
        System.out.println(isPrime(7));
        System.out.println(isPrime(8));
        System.out.println(isPrime(11));
        System.out.println(isPrime(13));
        System.out.println(isPrime(157));
        System.out.println(isPrime(158));

    }

    public static boolean isPrime (int x) {
        if (x < 2) {
            return false;
        }

        else {
            for (int i = 2; i <= (int) Math.sqrt(x); i++) {
                if (x % i == 0) return false;
            }
            return true;
        }


    }
}
