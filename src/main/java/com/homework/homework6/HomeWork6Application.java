package com.homework.homework6;

import com.homework.homework6.app.BookDaoBean;
import com.homework.homework6.app.UserDaoBean;
import com.homework.homework6.model.Book;
import com.homework.homework6.model.User;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.time.LocalDate;
import java.util.Date;

@SpringBootApplication
public class HomeWork6Application implements CommandLineRunner  {
    private final BookDaoBean bookDaoBean;
    private final UserDaoBean userDaoBean;

    public HomeWork6Application(BookDaoBean bookDaoBean, UserDaoBean userDaoBean) {
        this.bookDaoBean = bookDaoBean;
        this.userDaoBean = userDaoBean;
    }

    public static void main(String[] args) {
        SpringApplication.run(HomeWork6Application.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        userDaoBean.addUser(new User("Иванов", "Иван", LocalDate.of(1981, 1, 1),
                "+7-916-555-5555", "ivanov@email.com", "Дракула, Вий, Сияние"));
        userDaoBean.addUser(new User("Петров", "Петр", LocalDate.of(1983, 3, 3),
                "+7-910-555-5555", "petrov@email.com", "Мы, 1984, Кысь"));
        userDaoBean.addUser(new User("Сидоров", "Иван", LocalDate.of(1981, 1, 1),
                "+7-925-555-5555", "sidorov@email.com", "Ночь в Лиссабоне, По ком звонит колокол," +
                " А зори здесь тихие"));
        bookDaoBean.addBook(new Book("Дракула", "Стокер", LocalDate.now()));
        bookDaoBean.addBook(new Book("Вий", "Гоголь", LocalDate.now()));
        bookDaoBean.addBook(new Book("Сияние", "Кинг", LocalDate.now()));
        bookDaoBean.addBook(new Book("Мы", "Замятин", LocalDate.now()));
        bookDaoBean.addBook(new Book("1984", "Оруэлл", LocalDate.now()));
        bookDaoBean.addBook(new Book("Кысь", "Толстая", LocalDate.now()));
        bookDaoBean.addBook(new Book("Ночь в Лиссабоне", "Ремарк", LocalDate.now()));
        bookDaoBean.addBook(new Book("По ком звонит колокол", "Хэмингуэй", LocalDate.now()));
        bookDaoBean.addBook(new Book("А зори здесь тихие", "Васильев", LocalDate.now()));

        bookDaoBean.getInfo(userDaoBean.findUserByPhone("+7-925-555-5555"));

    }
}
