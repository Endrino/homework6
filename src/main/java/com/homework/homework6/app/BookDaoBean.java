package com.homework.homework6.app;

import com.homework.homework6.model.Book;
import com.homework.homework6.model.User;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;

@Component
public class BookDaoBean {
    
    private final Connection connection;
    
    public BookDaoBean(Connection connection) {
        this.connection = connection;
    }
    
    public Book findBookByTitle(String bookTitle) throws SQLException {
        PreparedStatement selectQuery = connection.prepareStatement("select * from books where title = ?");
        selectQuery.setString(1, bookTitle);
        ResultSet resultSet = selectQuery.executeQuery();
        Book book = new Book();
        while (resultSet.next()) {
            book.setBookAuthor(resultSet.getString("author"));
            book.setBookTitle(resultSet.getString("title"));
            book.setDateAdded(resultSet.getDate("date").toLocalDate());
        }
        System.out.println(book);
        return book;
    }

    public void getInfo (User user) throws SQLException {
        String[] bookList = user.getBookList().split(", ");
        for (String bookTitle : bookList) {
            findBookByTitle(bookTitle);
        }
    }

    public void addBook(Book book) throws SQLException {
        PreparedStatement insertQuery = connection.prepareStatement("insert into books (title, author, date)" +
                "VALUES (?, ?, ?)");
        insertQuery.setString(1, book.getBookTitle());
        insertQuery.setString(2, book.getBookAuthor());
        insertQuery.setDate(3, Date.valueOf(book.getDateAdded()));
        insertQuery.executeUpdate();
    }


}
