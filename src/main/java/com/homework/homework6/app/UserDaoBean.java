package com.homework.homework6.app;

import com.homework.homework6.model.User;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.sql.*;

@Component
@Scope("prototype")
public class UserDaoBean {

    private final Connection connection;

    public UserDaoBean(Connection connection) {
        this.connection = connection;
    }
    
    public User findUserByPhone(String userPhone) throws SQLException {
        PreparedStatement selectQuery = connection.prepareStatement("select * from users where phone = ?");
        selectQuery.setString(1, userPhone);
        ResultSet resultSet = selectQuery.executeQuery();
        User user = new User();
        while (resultSet.next()) {
            user.setUserName(resultSet.getString("name"));
            user.setUserSurname(resultSet.getString("surname"));
            user.setUserPhone(resultSet.getString("phone"));
            user.setBookList(resultSet.getString("books"));

            // System.out.println(user);
        }
        return user;
    }

    public void addUser(User user) throws SQLException {
        PreparedStatement insertQuery = connection.prepareStatement("insert into users (surname, name," +
                "birthdate, phone, email, books) VALUES (?, ?, ?, ?, ?, ?)");
        insertQuery.setString(1, user.getUserSurname());
        insertQuery.setString(2, user.getUserName());
        insertQuery.setDate(3, Date.valueOf(user.getBirthDate()));
        insertQuery.setString(4, user.getUserPhone());
        insertQuery.setString(5, user.getUserEmail());
        insertQuery.setString(6, user.getBookList());
        insertQuery.executeUpdate();
    }

}
