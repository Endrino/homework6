package com.homework.homework6.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDate;

@Entity
@Table(name = "books")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString


@SequenceGenerator(name = "default_gen", sequenceName = "books_id_seq", allocationSize = 1)
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "id")
public class Book
      extends GenericModel {
    
    @Column(name = "title", nullable = false)
    private String bookTitle;

    @Column(name = "author")
    private String bookAuthor;

    @Column(name = "date", nullable = false)
    private LocalDate dateAdded;


}
