package com.homework.homework6.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDate;
import java.util.Date;

@Entity
@Table(name = "users")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString


@SequenceGenerator(name = "default_gen", sequenceName = "users_id_seq", allocationSize = 1)
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "id")
public class User
      extends GenericModel {

    @Column(name = "surname", nullable = false)
    private String userSurname;
    
    @Column(name = "name", nullable = false)
    private String userName;

    @Column(name = "birthdate", nullable = false)
    private LocalDate birthDate;
    
    @Column(name = "phone", nullable = false)
    private String userPhone;

    @Column(name = "email", nullable = false)
    private String userEmail;
    
    @Column(name = "books", nullable = false)
    private String bookList;
}
