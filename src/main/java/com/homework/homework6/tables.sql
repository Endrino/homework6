create table users (
                       id bigserial primary key,
                       surname varchar(30) not null,
                       name varchar(30) not null,
                       birthdate date not null,
                       phone varchar(30) not null,
                       email varchar(30) not null,
                       books varchar(100)
);

create table books (
                       id bigserial primary key,
                       title varchar(30) not null,
                       author varchar(30) not null,
                       date date not null
);
